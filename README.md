# MFCFFmpeg

#### 介绍
在windows的MFC下使用ffmpeg的演示程序，播放rtsp流与保存视频流

#### 软件架构

ffmpeg: ffmpeg-4.4-full_build-shared

https://www.gyan.dev/ffmpeg/builds/packages/ffmpeg-4.4-full_build-shared.7z

rtmpdump=> librtmp

https://gitee.com/rzkn/rtmpdump.git

使用大白兔测试地址（H264)：

rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov

本程序支撑H265的RTMP推流

#define MEDIA_DATA_H264 7
#define MEDIA_DATA_H265 12

H265在网络上基本上都默认为12


#### 关于流媒体服务器

推荐： https://gitee.com/xia-chu/ZLMediaKit

nginx扩展: 

https://gitee.com/winshining/nginx-http-flv-module.git  不支撑H265

https://gitee.com/xiongliang/nginx-rtmp-module.git  支撑了H265


### 增加Onvif取流操作（只支持digest认证)

