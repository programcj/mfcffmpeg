// RTMPPushDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MFCFFMPEG.h"
#include "RTMPPushDlg.h"
#include "afxdialogex.h"

#include "rtmppush/RTMPPush.h"

#include <mutex>
#include <list>


static std::list<RTMPPush *> list;
static std::mutex mutex;

void RTMPPushAdd(RTMPPush *push)
{
	mutex.lock();
	list.push_back(push);
	mutex.unlock();
}

void RTMPPushDel(RTMPPush *push)
{
	std::list<RTMPPush *>::iterator itr;

	mutex.lock();
	for (itr = list.begin(); itr != list.end();)
	{
		if ((*itr) == push)
		{
			//list.remove(push);
			list.erase(itr++);
			continue;
		}
		itr++;
	}
	mutex.unlock();
}

void RTMPPushStopAll()
{
	std::list<RTMPPush *>::iterator itr;

	mutex.lock();
	for (itr = list.begin(); itr != list.end();)
	{
		(*itr)->stop();
		itr++;
	}
	mutex.unlock();
}

// RTMPPushDlg 对话框

#define WM_MY_MESSAGE (WM_USER+100)         //WM_USER为windows系统为非系统消息保留的ID，这里至少要用100，因为其它控件的消息会占用一部分。

IMPLEMENT_DYNAMIC(RTMPPushDlg, CDialogEx)

RTMPPushDlg::RTMPPushDlg(CWnd *pParent /*=NULL*/)
	: CDialogEx(RTMPPushDlg::IDD, pParent), m_urlIn(_T("")), m_urlOut(_T(""))
{
}

RTMPPushDlg::~RTMPPushDlg()
{
}

void RTMPPushDlg::DoDataExchange(CDataExchange *pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_urlIn);
	DDX_Text(pDX, IDC_EDIT2, m_urlOut);
}

BEGIN_MESSAGE_MAP(RTMPPushDlg, CDialogEx)
ON_WM_CLOSE()
ON_BN_CLICKED(IDC_BUTTON_StartPush, &RTMPPushDlg::OnBnClickedButtonStartpush)
ON_BN_CLICKED(IDC_BUTTON_StopPush, &RTMPPushDlg::OnBnClickedButtonStoppush)
ON_MESSAGE(WM_MY_MESSAGE, &RTMPPushDlg::OnMyMessage)
END_MESSAGE_MAP()

// RTMPPushDlg 消息处理程序

BOOL RTMPPushDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	SetDlgItemText(IDC_EDIT1, L"save.264");
	SetDlgItemText(IDC_EDIT2, L"rtmp://");
	
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}

//模态对话框 释放内存:1
void RTMPPushDlg::OnClose()
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	CDialogEx::OnClose();
	RTMPPushStopAll();
	Sleep(100);
	DestroyWindow();
}

//模态对话框 释放内存:2
void RTMPPushDlg::PostNcDestroy()
{
	// TODO:  在此添加专用代码和/或调用基类
	CDialogEx::PostNcDestroy();
	delete this;
}


UINT _ThreadPush(LPVOID lparam) //线程1函数定义
{
	RTMPPush *push = (RTMPPush *)lparam;

	RTMPPushDlg *dlg = (RTMPPushDlg*)push->getBindObj();
	RTMPPushAdd(push);
	push->start([=](const char *msg){
		dlg->SendMessage(WM_MY_MESSAGE, WM_MY_MESSAGE, (LPARAM)msg);
	});
	RTMPPushDel(push);
	delete push;
	return 0;
}

void RTMPPushDlg::OnBnClickedButtonStartpush()
{
	UpdateData(true);

	USES_CONVERSION;

	char *urlin = T2A(m_urlIn.GetBuffer(0));
	char *urlout = T2A(m_urlOut.GetBuffer(0));

	//char* pData = "1234";
	//CString strData(pData);

	RTMPPush *push = new RTMPPush(urlin, urlout);
	push->setBindObj(this);
	AfxBeginThread(_ThreadPush, push, THREAD_PRIORITY_NORMAL);
}

void RTMPPushDlg::OnBnClickedButtonStoppush()
{
	RTMPPushStopAll();
}


afx_msg LRESULT RTMPPushDlg::OnMyMessage(WPARAM wParam, LPARAM lParam)
{
	const char *msgptr = (const char *)lParam;
	CString msg(msgptr);

	SetDlgItemText(IDC_EDIT_MSG, msg);
	return 0;
}

