﻿#pragma once


extern "C" {
#include "libonvif/SOAP_Onvif.h"
}



struct URLInfo
{
	char prefix[10];
	char host[30];
	int port;
	char useranme[30];
	char password[30];
	const char* path;
};

int urldecode(const char* url, struct URLInfo* urlinfo);

class OnvifInfoDialog;

class OnvifDevice {

public:
	char uri[500];
	char username[50];
	char password[50];
	BOOL ipreplaceflag;

	HWND m_hWnd;

	//设备信息
	struct OnvifDeviceInformation devInfo;

	//MediaURL
	char mediaUrl[300];

	//流
	struct OnvifDeviceProfiles profiles[5];
	char urls[5][300];

	char stat[300];

	OnvifDevice(HWND hWnd, const char* uri, const char* username, const char* password, bool replaceipflag) {
		this->m_hWnd = hWnd;
		strcpy(this->uri, uri);
		strcpy(this->username, username);
		strcpy(this->password, password);
		ipreplaceflag = replaceipflag;

		char* url = strchr(this->uri, ' ');
		if (url)
			*url = 0;

		memset(&devInfo, 0, sizeof(devInfo));
		memset(mediaUrl, 0, sizeof(mediaUrl));
		memset(profiles, 0, sizeof(profiles));
		memset(urls, 0, sizeof(urls));
		memset(stat, 0, sizeof(stat));
	}

	int GetDeviceInformation()
	{
		int ret = Onvif_GetDeviceInformation(uri, username, password, &devInfo);
		if (ret != 200)
			sprintf(stat, "GetDeviceInformation: %d", ret);
		return ret;
	}

	int GetCapabilities_Media() {		
		int ret = Onvif_GetCapabilities_Media(uri, username, password, mediaUrl);
		if (ret != 200)
			sprintf(stat, "GetCapabilities Media: %d", ret);
		else {
			if (ipreplaceflag) {
				struct URLInfo urlinfo_media;
				struct URLInfo urlinfo_uri;

				memset(&urlinfo_media, 0, sizeof(urlinfo_media));
				memset(&urlinfo_uri, 0, sizeof(urlinfo_uri));

				urldecode(mediaUrl, &urlinfo_media);
				urldecode(uri, &urlinfo_uri);

				if (strcmp(urlinfo_media.host, urlinfo_uri.host) != 0 || urlinfo_media.port != urlinfo_uri.port)
				{
					printf("不一样\r\n");
					char url[300];
					sprintf(url, "%s%s:%d%s", urlinfo_uri.prefix, urlinfo_uri.host, urlinfo_uri.port, urlinfo_media.path);
					printf("新地址:%s\r\n", url);
					strcpy(mediaUrl, url);
				}
				else {
					printf("media 一样\r\n");
				}
			}
		}
		return ret;
	}

	int MediaService_GetProfiles() {		

		int ret = Onvif_MediaService_GetProfiles(mediaUrl, username, password, profiles);
		if (ret != 200) {		
			sprintf(stat, "MediaService GetProfiles: %d", ret);
		}
		return ret;
	}

	int MediaServer_GetStreamUri() {
		int i;
		int ret;

		for (i = 0; i < 5; i++)
		{
			if (strlen(profiles[i].token) == 0)
				break;
			//printf("token:%s, %dx%d, %s\r\n", profiles[i].token, profiles[i].width, profiles[i].height, profiles[i].encodname);
			memset(urls[i], 0, 300);

			ret = Onvif_MediaServer_GetStreamUri(mediaUrl, username,
				password, profiles[i].token, urls[i]);
			OnvifURI_Decode(urls[i], urls[i], 300);
		}
		if (ret != 200)
			sprintf(stat, "MediaServer GetStreamUri: %d", ret);
		return ret;
	}

	const char* getStatStr() {
		return stat;
	}
};
// OnvifInfoDialog 对话框


class OnvifInfoDialog : public CDialogEx
{
	DECLARE_DYNAMIC(OnvifInfoDialog)

public:
	OnvifInfoDialog(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~OnvifInfoDialog();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_ONVIF_INFO };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	virtual void PostNcDestroy();
	CString m_devURI;
	afx_msg void OnBnClickedButtonDevfind();
	virtual BOOL OnInitDialog();
	
	CString m_UserName;
	CString m_Password;
	CString m_onvifInfoText;

	OnvifDevice* _onvifDevice;

	void setOnvifDevice(OnvifDevice *device) {
		if (_onvifDevice != NULL)
			delete _onvifDevice;
		_onvifDevice = device;
	}
protected:
	afx_msg LRESULT OnOnvifInfoMsg(WPARAM wParam, LPARAM lParam);
public:
	CStatusBarCtrl m_statusBar;
	CComboBox m_BoxURL;
	afx_msg void OnBnClickedButtonPlay();
	BOOL m_OneOnvifIP;
	afx_msg void OnBnClickedButtonOnvifSearch();
	// 本机IP地址
	CComboBox m_ComBoxNetIP;
	CListCtrl m_uiListOnvif;
	afx_msg void OnNMDblclkListOnvifDevice(NMHDR* pNMHDR, LRESULT* pResult);
};
