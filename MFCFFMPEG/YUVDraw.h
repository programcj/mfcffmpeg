#pragma once
#include <afxwin.h>

#define MYWNDCLASS L"YUVDraw" 

class YUVDraw :  public CWnd
{
public:
	YUVDraw();

	BOOL RegisterWndClass();

	afx_msg void OnPaint();

	BOOL OnEraseBkgnd(CDC* pDC);

	void showBitmap(HBITMAP bitmap, int w, int h);

protected:
	HBITMAP m_hbitmap[3];

	int m_width;
	int m_height;

	DECLARE_MESSAGE_MAP()
};

