﻿#pragma once


// GB28181Dialog 对话框

class GB28181Dialog : public CDialogEx
{
	DECLARE_DYNAMIC(GB28181Dialog)

	CWinThread* m_pthread;
public:
	GB28181Dialog(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~GB28181Dialog();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_GB28181 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CComboBox m_myips;
	afx_msg void OnBnClickedButtonStart();
	int m_port;
	int m_RTPPSPT;
	int m_RTP_PT_H264;
	int m_RTP_PT_H265;
};
