#pragma once


// RTMPPushDlg 对话框

class RTMPPushDlg : public CDialogEx
{
	DECLARE_DYNAMIC(RTMPPushDlg)

public:
	RTMPPushDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~RTMPPushDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_RTMPPUSH };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClose();
	virtual void PostNcDestroy();
	afx_msg void OnBnClickedButtonStartpush();
	afx_msg void OnBnClickedButtonStoppush();
	CString m_urlIn;
	// 推流地址
	CString m_urlOut;
protected:
	afx_msg LRESULT OnMyMessage(WPARAM wParam, LPARAM lParam);
public:
	virtual BOOL OnInitDialog();
};
