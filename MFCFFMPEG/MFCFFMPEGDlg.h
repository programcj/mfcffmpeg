﻿
// MFCFFMPEGDlg.h: 头文件
//

#pragma once

#include "YUVDraw.h"

// CMFCFFMPEGDlg 对话框
class CMFCFFMPEGDlg : public CDialogEx
{
// 构造
public:
	CMFCFFMPEGDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFCFFMPEG_DIALOG };
#endif

	CString m_url;
	int m_threadLoop;
	YUVDraw m_yuvDraw;

	void putLog(CString str);
	void showStreamInfo(CString infoStr);
	void putRGB24Data(void* data, int w, int h);

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;
	

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	afx_msg LRESULT OnUserThreadend(WPARAM wParam, LPARAM lParam);
	

public:
	afx_msg void OnBnClickedButtonPlay();
	CString m_SaveFileName;
	afx_msg void OnMenuRtmppush();
	afx_msg void OnMenuOnvifInfo();
	afx_msg void OnMenuGb28181();
};
