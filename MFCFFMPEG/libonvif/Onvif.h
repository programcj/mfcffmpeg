#ifndef __LIB_ONVIF_H_
#define __LIB_ONVIF_H_

#define ONVIF_COMMON_STR_LEN 128
#define ONVIF_INFO_SERVER_IP_LEN 50
#define ONVIF_RTSP_URL_LEN 256
#define ONVIF_MAX_DISCORVER_IPC 128

typedef struct
{
    char uuid[ONVIF_COMMON_STR_LEN];
    char serverAddress[ONVIF_COMMON_STR_LEN];
    char serverIP[ONVIF_INFO_SERVER_IP_LEN];
    int hikDeviceFlag;
}ONVIF_DISCORVER_IPC_DATA_S;

typedef struct 
{
    int num;
    ONVIF_DISCORVER_IPC_DATA_S data[ONVIF_MAX_DISCORVER_IPC];
}ONVIF_DISCOVER_IPCS_S;

typedef struct
{
    char brand[ONVIF_COMMON_STR_LEN];
    char model[ONVIF_COMMON_STR_LEN];
    char serialnumber[ONVIF_COMMON_STR_LEN];
    char firmwareversion[ONVIF_COMMON_STR_LEN];
}ONVIF_IPC_DEV_DATA;

typedef struct
{
    char rtspUrl[ONVIF_RTSP_URL_LEN]; //master
    char rtspUrlSub[ONVIF_RTSP_URL_LEN]; //子流
}ONVIF_IPC_URL_DATA;

typedef struct
{
    ONVIF_IPC_DEV_DATA devData;
    ONVIF_IPC_URL_DATA url;
}ONVIF_IPC_DATA_S;


int OnvifGetIpcData (char *url, char *usename, char *password, ONVIF_IPC_DATA_S *ipcData);
int OnvifGetIpcDataByIp (const char *ip, const char *usename, const char *password, ONVIF_IPC_DATA_S *ipcData);
int IpcProbe (char *devServiceAddrBuf, int devServiceAddrBufSize, char *ip);

#include <stdint.h>

//搜索局域网内所有的IPC,当前默认广播地址为255.255.255.255
void OnvifDiscoverAll(int timeoutsec,
		void (*bkfun)(void *context, uint32_t addr, const char *XAddrs),
		void *context);

//onvif检查，是否能读取信息，可以用于用户名密码校验
int OnvifCheck(const char *url, const char *username, const char *password);

#endif
