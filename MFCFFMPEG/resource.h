//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 MFCFFMPEG.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MFCFFMPEG_DIALOG            102
#define IDS_STRING_RTMPPUSH             102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDS_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_RTMPPUSH             130
#define IDR_MENU1                       131
#define IDD_DIALOG_ONVIF_INFO           132
#define IDD_DIALOG_GB28181              134
#define IDC_EDIT_URL                    1000
#define IDC_BUTTON_PLAY                 1001
#define IDC_STATIC_STATUS               1003
#define IDC_STATIC_INFO                 1004
#define IDC_CUSTOM1                     1005
#define IDC_EDIT1                       1006
#define IDC_EDIT_SAVENAME               1006
#define IDC_BUTTON_StartPush            1007
#define IDC_EDIT_MYPORT                 1007
#define IDC_BUTTON_StopPush             1008
#define IDC_EDIT2                       1009
#define IDC_EDIT3                       1010
#define IDC_EDIT_MSG                    1010
#define IDC_EDIT_PASSWORD               1010
#define IDC_BUTTON_DEVFIND              1012
#define IDC_EDIT_DEVURI                 1013
#define IDC_EDIT_USERNAME               1014
#define IDC_EDIT_INFO                   1015
#define IDC_COMBO_PLAYURL               1017
#define IDC_CHECK_ONE_ONVIF_IP          1019
#define IDC_BUTTON1                     1020
#define IDC_BUTTON_ONVIF_SEARCH         1020
#define IDC_BUTTON_START                1020
#define IDC_COMBO_NET_IP                1021
#define IDC_LIST_ONVIF_DEVICE           1022
#define IDC_COMBO_MYIPS                 1023
#define IDC_EDIT_RTP_PT_PS              1024
#define IDC_EDIT_RTP_PT_H264            1025
#define IDC_EDIT_RTP_PT_H265            1026
#define ID_32771                        32771
#define IDM_RTMPPUSH                    32772
#define ID_MENU_RTMPPUSH                32773
#define ID_32774                        32774
#define IDM_ONVIF_INFO                  32775
#define ID_MENU_ONVIF_INFO              32776
#define ID_32777                        32777
#define ID_MENU_GB28181                 32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
