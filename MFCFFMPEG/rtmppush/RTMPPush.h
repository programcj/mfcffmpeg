#pragma once

#include <Windows.h>

#include <functional>

class RTMPPush
{
	char urlin[1024];
	char urlout[1024];
	bool _loop;
	char stat[300];

	void *bindobj;
public:
	
	RTMPPush(const char* urlin, const char* urlout);

	~RTMPPush();

	bool start(std::function<void(const char *msg)>f);

	void stop();

	const char* getURLOut();
	const char* getStatStr();

	void setBindObj(void *obj);
	void *getBindObj();
};

