#include "pch.h"
#include "YUVDraw.h"

BEGIN_MESSAGE_MAP(YUVDraw, CWnd)
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
END_MESSAGE_MAP()

YUVDraw::YUVDraw()
{
	RegisterWndClass();
	m_hbitmap[0] = NULL;
	m_hbitmap[1] = NULL;
	m_hbitmap[2] = NULL;
}

BOOL YUVDraw::RegisterWndClass()
{
	WNDCLASS windowclass;
	HINSTANCE hInst = AfxGetInstanceHandle();

	if (!(::GetClassInfo(hInst, MYWNDCLASS, &windowclass)))
	{
		windowclass.style = CS_DBLCLKS;
		windowclass.lpfnWndProc = ::DefWindowProc;
		windowclass.cbClsExtra = windowclass.cbWndExtra = 0;
		windowclass.hInstance = hInst;
		windowclass.hIcon = NULL;
		windowclass.hCursor = AfxGetApp()->LoadStandardCursor(IDC_ARROW);
		windowclass.hbrBackground = ::GetSysColorBrush(COLOR_WINDOW);
		windowclass.lpszMenuName = NULL;
		windowclass.lpszClassName = MYWNDCLASS;

		if (!AfxRegisterClass(&windowclass))
		{
			AfxThrowResourceException();
			return FALSE;
		}
	}
	return TRUE;
}

void YUVDraw::OnPaint()
{
	CPaintDC dc(this); // 用于绘制的设备上下文
	if (m_hbitmap[0])
	{	

		CRect rcClient;
		GetClientRect(rcClient);

		CDC memDc;
		memDc.CreateCompatibleDC(&dc);
		memDc.SelectObject(m_hbitmap[0]);
		SetStretchBltMode(dc.m_hDC, HALFTONE);

		dc.SetStretchBltMode(COLORONCOLOR);
		//dc.StretchBlt(0, 0, rcClient.Width(), rcClient.Height(), &memDc, 0, 0, m_width, m_height, SRCCOPY);

		dc.StretchBlt(0, 0, rcClient.Width(), rcClient.Height(), &memDc, 0, m_height-1, m_width, -m_height, SRCCOPY);

		// 用下句翻转一次图像否则会出现垂直翻转现象
		//    StretchBlt(MemDC, 0, 0, bm.bmWidth, bm.bmHeight,
		//        MemDC, 0, bm.bmHeight-1,   bm.bmWidth,   -bm.bmHeight,   SRCCOPY);

		memDc.DeleteDC();
	}
}

BOOL YUVDraw::OnEraseBkgnd(CDC* pDC)
{
	return TRUE;//直接返回真
 //return CWnd::OnEraseBkgnd(pDC);//禁止父类擦除背景，重画。
}

void YUVDraw::showBitmap(HBITMAP bitmap, int w, int h)
{
	if (m_hbitmap[0])
	{
		::DeleteObject(m_hbitmap[0]);
		m_hbitmap[0] = NULL;
	}
	
	m_hbitmap[0] = bitmap;

	m_width = w;
	m_height = h;

	Invalidate();	
}
